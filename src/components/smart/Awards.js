import React, { Component } from "react";
import Articles from "../Reusable/Articles";
const awards=[
    {
      year: "2020", 
      title: "Test of English for International Communication (TOEIC)",
      contents: "Apte à comprendre, parler et s'exprimer sur des sujets familiers et quotidien (TOEIC Niveau B1)."
    },
    {
      year: "2020", 
      title: "Diplôme d'Etude en Langue Française DELF B2",
      contents: "Preuve d'un savoir-faire pour s'exprimer, argumenter et d'une capacité à s'autocorriger."
    },
    {
      year: "2020", 
      title: "Licence professionnelle en Système Informatique et Logiciel (SIL)",
      contents: "Diplôme nationale béninois attestant des capacités à faire face à divers cas concrets et à conduire des projets professionnels complets."
    },
];
class Formation extends Component {
  render(){
    return (
      <article className="i_am_doing">
          {Articles("Diplômes",awards)}
      </article>
    )
  }
}
export default Formation;