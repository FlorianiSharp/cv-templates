import React, { Component } from "react";
import Skills from "../Reusable/Skills";
const language=[
  {
    name: "Anglais", 
    rate: "70%",
  },
  {
    name: "Français", 
    rate: "90%",
  },
];
class Language extends Component {
  render(){
    return (
      <article className="category">
        {Skills("Langues",language)}
      </article>
    )
  }
}
export default Language;