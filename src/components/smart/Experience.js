import React, { Component } from "react";
import Articles from "../Reusable/Articles";
const history=[
    {
      year: "2021-Ajd", 
      title: "Développeur Odoo - RINTIO - Zogbo (Cotonou - Bénin) ",
      contents: "Analyse du fonctionnement des modules Odoo pour la conception, le développement de modules capables d'améliorer les possibilités des modules existants et de créer des solutions nouvelles."
    },
    {
      year: "2019-Ajd", 
      title: "Intégrateur Web",
      contents: "Intégration de templates de CV et déploiement sur GitHub/Gitlab."
    },
    {
      year: "2020-2021", 
      title: "Développeur Web - RINTIO - Zogbo (Cotonou - Bénin)",
      contents: "Utilisation d'outils et Framework pour le développement et la maintenance de site web (Bootstrap, VueJs, WordPress)."
    },
    {
      year: "2019-2020", 
      title: "Conception d’une plateforme de gestion des marchés par appel d’offres - Projet de Fin de Mémoire de Licence -  PIGIER- Bénin (Cotonou)",
      contents: "Automatiser et informatiser les différents processus de passation des marchés par appel d'offres."
    },
];
class Experience extends Component {
  render(){
    return (
      <article className="i_am_doing">
          {Articles("Experience Professionelle",history)}
      </article>
    )
  }
}
export default Experience;