import React, { Component } from "react";
class MoreInformation extends Component {
  render(){
    return (
      <article className="more">
        <h3 className="this">Complément d'infos</h3>
        <p className="description">
          Passionné de programmation, j'aime lire, découvrir de nouvelles technologies et surtout donner vie aux réalisations graphiques.
          <br/>
          Voici quelques liens utiles relatant mes oeuvres :<br/>
          <strong>NB :</strong>&nbsp;Les informations présentées à partir de ces liens sont soumis à des règles de confidentialités et ne sont pas à prendre à la lettre
        </p>
        <ul title="Aperçu" className="links_proofs">
          <li>
            Curriculum Vitae - TIGRI Amadéus Friant Fleury (Ce CV avec ReactJs)
            <a href="https://jigsaw.w3.org/css-validator/check/referer"
                className="css_validator">
                <img src="https://jigsaw.w3.org/css-validator/images/vcss-blue"
                    alt="CSS Valide !" />
            </a>
            <ul>
              <li>
                <a href="https://gitlab.com/FlorianiSharp/cv-templates.git">
                  Voir sur Gitlab
                </a>
              </li>
              <li>
                <a href="https://react-cv-templates.herokuapp.com/">
                  Visionner directement sur Heroku
                </a>
              </li>
            </ul>
          </li>
          <li>
              CV Belgie - ADIMOU
            <ul>
              <li>
                <a href="https://github.com/CurriculumVitaeOnline/Belgie_ADIMOU.git">
                  Voir sur GitHub
                </a>
              </li>
              <li>
                <a href="https://curriculumvitaeonline.github.io/Belgie_ADIMOU/">
                  Visionner directement sur GitHub.io
                </a>
              </li>
            </ul>
          </li>
          <li>
              Restaurant Ohmyfood
            <ul>
              <li>
                <a href="https://github.com/FleuryTIGRIOpenclassrooms/P3_tigri_fleury.git">
                  Voir sur GitHub
                </a>
              </li>
              <li>
                <a href="https://fleurytigriopenclassrooms.github.io/P3_tigri_fleury/">
                  Visionner directement sur GitHub.io
                </a>
              </li>
            </ul>
          </li>
          <li>
              CV - Gerard Hessanon 
            <ul>
              <li>
                <a href="https://github.com/CurriculumVitaeOnline/GerardHessanon.git">
                  Voir sur GitHub
                </a>
              </li>
              <li>
                <a href="https://curriculumvitaeonline.github.io/GerardHessanon/">
                  Visionner directement sur GitHub.io
                </a>
              </li>
            </ul>
          </li>
          <li>
              CV - Fleury TIGRI
            <ul>
              <li>
                <a href="https://github.com/FleuryTIGRIOpenclassrooms/P2_tigri_fleury.git">
                  Voir sur GitHub
                </a>
              </li>
              <li>
                <a href="https://fleurytigriopenclassrooms.github.io/P2_tigri_fleury/">
                  Visionner directement sur GitHub.io
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </article>
    )
  }
}
export default MoreInformation;