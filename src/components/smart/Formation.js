import React, { Component } from "react";
import Articles from "../Reusable/Articles";
const formations=[
    {
      year: "2020", 
      title: "Licence professionnelle en Réseaux et Génie Logicielle - PIGIER Bénin",
      contents: "Capable de maîtriser les techniques d’ingénierie liées à la conception des systèmes logiciels, ainsi que la conception, le déploiement et l’administration d’un réseau informatique local d’entreprise."
    },
    {
      year: "2018", 
      title: "Développeur Front End - OpenClassrooms ",
      contents: "Acquisition des capacités pour le découpage, l'intégration et la dynamisation de pages web (HTML5, CSS3, SASS, JavaScript ES6)."
    },
];
class Formation extends Component {
  render(){
    return (
      <article className="i_am_doing">
          {Articles("Formation",formations)}
      </article>
    )
  }
}
export default Formation;