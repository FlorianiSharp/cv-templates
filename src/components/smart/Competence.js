import React, { Component } from "react";
import Skills from "../Reusable/Skills";
const competences=[
  {
    name: "Intégration (HTML5 - CSS3)", 
    rate: "90%",
  },
  {
    name: "Dynamisation (JavaScript ES6)", 
    rate: "80%",
  },
  {
    name: "Technologies (SASS - ReactJs)", 
    rate: "70%",
  },
];
class Competences extends Component {
  render(){
    return (
      <article className="category">
        {Skills("Compétences clés",competences)}
      </article>
    )
  }
}
export default Competences;