const Articles  = (name,contents_array) =>{
    const contents = [
        <h3 className="this">{name}</h3>
    ];
    for(let i = 0; i < contents_array.length; i++) {
        contents.push(
            <aside className="since">
                <span className="year">{contents_array[i].year}</span>
                <p className="doing">
                    <strong className="where">
                        {contents_array[i].title}
                    </strong>
                    {contents_array[i].contents}
                </p>
            </aside>
        );
    }
    return contents;
}
export default Articles;  