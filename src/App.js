import React, { Component } from "react";
import Header from "./layouts/Header";
import Awards from "./components/smart/Awards";
import Formation from "./components/smart/Formation";
import Experience from "./components/smart/Experience";
import Language from "./components/smart/Language";
import Competence from "./components/smart/Competence";
import MoreInformations from "./components/smart/More_informations";
import "./css/style.css";
class App extends Component {
  render(){
    return (
      <section className="main_contents">
        <Header/>
        <section className="contents">
          <Awards/>
          <Formation/>
          <Experience/>
          <section className="skills">
            <Language/>
            <Competence/>
          </section>
          <MoreInformations/>
        </section>
      </section>
    )
  }
}
export default App;