import React, { Component } from "react";
import picture from "../images/tigri_amadeus_friant_fleury.jpg";
import picture_full from "../images/tigri_amadeus_friant_fleury_full.jpg";
import cv_pdf from "../docs/curriculum_vitae_tigri_amadeus_friant_fleury.pdf";
import { IoMdDownload } from "react-icons/io";

class Header extends Component {
  render(){
    return (
      <header className="personnal_informations">
        <figure className="me">
          <a href={picture_full} title="Cliquez pour agrandir" className="picture">
            <img src={picture} title="Cliquez pour agrandir" alt="Profil"/>
          </a>
          <figcaption className="description">
            <h1 className="name">Amadéus Friant Fleury <span className="lastname">TIGRI</span></h1>
            <h2 className="function">Développeur Front End 
              <a href={cv_pdf} title="Cliquez pour télécharger">
                <IoMdDownload title="Cliquez pour télécharger"/>
              </a>
            </h2>
          </figcaption>
        </figure>
        <address className="contact">
          <h3 className="my_contacts">CONTACT</h3>
          <a  href="https://maps.apple.com/?address=Rue%206.104,%20Cotonou,%20B%C3%A9nin&ll=6.385123,2.426134&q=Ma%20position&_ext=EiYplB2nJD2FGUAxlTLryyRgA0A5gsnVBXCOGUBBH0rm8qdyA0BQBA%3D%3D&t=m" 
              title="Voir sur la carte">
            <span className="at">Adresse :</span> COTONOU /CN° 1201- y Towéta 2
          </a>
          <a href="tel:+22996784583" title="Appel direct">
            <span className="at">Téléphone :</span> +22996784583
          </a>
          <a href="mailto:mail@gmail.com" title="Joignable également à cette adresse mail">
            <span className="at">Mail :</span> fleurytigri@yahoo.com
            </a>
        </address>
      </header>
    )
  }
}
export default Header;